Lizard is an 8chan Monitor. You give it a bunch of thread URLs, and it regularly checks these for you, letting you know if any have received new replies. The program can minimize to tray to run unobtrusively, and alerts you with balloon pop-ups. All new threads can be opened in the browser with a single, convenient command.

Thread watchers already exist, but I haven't seen one that doesn't run in a browser. Sometimes I have a thread on a slow board, and I don't want to bother remembering to check it every day or to keep a browser window open all the time (which would distract me from work). I wrote Lizard to solve this problem. Unlike browser-based solutions, Lizard:

* Keeps information stored on the hard drive, so that watched threads persist
* Uses less resources than a browser and doesn't get in your way
* Keeps a detailed log so you can tell what happened in what thread when
* (planned) Can make sure you get the latest replies to a thread even if it gets deleted while you are away

For details, see the wiki at: https://bitbucket.org/Thinty/lizard/wiki/Home