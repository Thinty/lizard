﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using LizardAPI;
using WinForms_GUI.Properties;

namespace WinForms_GUI
{
    public sealed partial class Main_Form : Form
    {
        private const string LOG_FILE_NAME = "Log.txt";
        private const string THREADS_FILE_NAME = "Threads.txt";

        private string version_string;

        public ThreadMonitor Monitor;

        private bool _monitoring;
        public bool Monitoring
        {
            get { return _monitoring; }
            set
            {
                _monitoring = value;

                var d = new Dictionary<bool, string> { { true, "enabled" }, { false, "disabled" } };
                Print_line("Monitoring " + d[value] + ".");

                btn_Start_monitoring.Enabled = !value;
                btn_Stop_monitoring.Enabled = value;

                ud_Interval.Enabled = !value;

                timer.Interval = (int)(1000 * ud_Interval.Value);
                timer.Enabled = value;

                tray_menu.MenuItems[0].Checked = Monitoring;
                tray_icon.Icon = value ? new Icon(Resources.lizard, 40, 40) : new Icon(Resources.lizardX, 40, 40);
            }
        }

        public Main_Form()
        {
            InitializeComponent();

            Print_line("Program launched.");

            // Help message
            tb_Help.Text = "To monitor threads, copy and paste the thread URL in the \"" + tabPage1.Text + "\" tab." +
                            "\r\n\r\nThe logger will notify you about any new replies, and give a link to the first new reply (the oldest one).\n\nThe program also creates two text files:" +
                            "\r\n* " + THREADS_FILE_NAME +
                            " is essentially a dump of the threads you are monitoring. When the program first runs, it checks for a " + THREADS_FILE_NAME +
                            " file, and if it finds one, attempts to load from it. The file then gets re-saved at every thread refresh." +
                            "\r\n* " + LOG_FILE_NAME +
                            " is essentially the same as the logger window. Every time there's a new log message, it is appended to the file." +
                            "\r\n\r\nDon't fuck with these files, and don't open them in any other programs that locks files (like Excel)." +
                            "\r\n\r\nThe program minimizes to a tray icon. New threads are announced with balloon pop-ups, clicking on balloon will open all threads with new replies." +
                            "\r\n\r\nLizard, the 8chan monitor. Version: ";

            Prepare_tray_icon();
#if DEBUG
            // Must be done after tray menu is initialized
            ud_Interval.Value = 5M;
#endif

            version_string = Version_Utilities.Extract_version_string(Assembly.GetExecutingAssembly());
            Text += version_string; // Sets main form title
            tb_Help.Text += version_string;

            // Initialize fields
            Monitor = new ThreadMonitor();

            // Load the threads file if it exists
            if (File.Exists(THREADS_FILE_NAME))
            {
                Print_line("Loading file " + THREADS_FILE_NAME);

                var raw_file = File.ReadAllLines(THREADS_FILE_NAME);
                Print_line("Found the following threads:");

                // Parse the file line by line
                foreach (var s in raw_file)
                {
                    var parts = s.Split('\t');

                    // This defines the format of the Threads file 
                    //TODO: Switch to JSON?
                    var url = parts[0];
                    var last_seen = long.Parse(parts[1]);

                    var t = new MonitoredThread(url, Print_line);
                    t.Last_seen_by_user = last_seen;

                    var human_time = DateTimeHelpers.From_unix_time(t.Last_seen_by_user).ToLocalTime();
                    Print_line("\t" + t.Name + ", timestamp " + t.Last_seen_by_user + " (" + human_time + ")");
                    Monitor.Threads.Add(t);
                }

                Print_line("Successfully loaded " + Monitor.Threads.Count + " threads from file.");
            }
            else
            {
                Print_line(THREADS_FILE_NAME + " not found.");
            }

            // Bind the data source
            dg_Threads.DataSource = Monitor.Threads;
        }

        #region Tray stuff
        private ContextMenu tray_menu = new ContextMenu();
        private NotifyIcon tray_icon;

        private void Prepare_tray_icon()
        {
            // Create a tray icon.
            tray_icon = new NotifyIcon
            {
                Text = "Lizard " + version_string,
                Icon = new Icon(Resources.lizardX, 16, 16)
            };

            // On click event
            EventHandler monitor_clicked = delegate { Monitoring = !Monitoring; };
            tray_icon.MouseClick += Tray_clicked;
            tray_icon.BalloonTipClicked += Balloon_tip_clicked;

            // Create a simple tray menu with only one item.
            tray_menu = new ContextMenu();
            tray_menu.MenuItems.Add("Monitor - " + ud_Interval.Value + " sec", monitor_clicked);
            tray_menu.MenuItems.Add("Open threads with new replies", Balloon_tip_clicked);
            tray_menu.MenuItems.Add("Open all threads", delegate { foreach (var t in Monitor.Threads) Process.Start(t.Full_url); });
            tray_menu.MenuItems.Add("Exit", delegate { Close(); });

            // Add menu to tray icon and show it.
            tray_icon.ContextMenu = tray_menu;
            tray_icon.Visible = true;
        }

        private void Tray_clicked(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Show();
                WindowState = FormWindowState.Normal;
            }
        }

        private void On_resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized) Hide();
        }
        #endregion

        private void Print_line(string s)
        {
            var message = "\r\n" + DateTime.Now + ": " + s;

            tb_Log.Text += message;
            File.AppendAllText(LOG_FILE_NAME, message);
        }

        private void Balloon_tip_clicked(object sender, EventArgs event_args)
        {
            var unix_now = DateTime.Now.To_unix_time();

            var urls = Monitor.Unread_thread_urls;
            foreach (var url in urls) Process.Start(url);

            Monitor.Update_last_seen_by_user(unix_now);
            var human_now = DateTimeHelpers.From_unix_time(unix_now).ToLocalTime();
            Print_line("Opened " + urls.Count() + " URLs in the browser. Timestamp updated to " + unix_now + " (" + human_now + ")");
        }

        private void Start_monitoring(object sender, EventArgs e)
        {
            Monitoring = true;
        }

        private void Stop_monitoring(object sender, EventArgs e)
        {
            Monitoring = false;
        }

        private void Refresh_monitored_threads(object sender, EventArgs e)
        {
            Print_line("Refreshing monitored threads now...");

            Monitor.Refresh_all_threads();

            var unread_threads = Monitor.Threads_with_unread_replies;
            if (unread_threads > 0) Announce_new_replies(unread_threads);
            Print_line("Finished checking threads. " + unread_threads + " threads have unread replies.");

            List<MonitoredThread> dead_threads = Monitor.Dead_threads;
            if (dead_threads.Any()) Print_line(dead_threads.Count + " threads are 404'd.");

            Save_threads_file();
        }

        private void Save_threads_file()
        {
            var urls = new List<string>();
            urls.AddRange(Monitor.Threads.Select(t => t.Full_url + "\t" + t.Last_seen_by_user));
            File.WriteAllLines(THREADS_FILE_NAME, urls);
        }

        private void Announce_new_replies(int count)
        {
            var title = count + " threads with unread replies";
            var tip_text = string.Join("\n", Monitor.Unread_thread_names);

            tray_icon.ShowBalloonTip(3000, title, tip_text, ToolTipIcon.Info);
            Print_line(title);
        }

        private void On_add_new_thread(object sender, EventArgs e)
        {
            Monitor.Threads.Add(new MonitoredThread(textBox3.Text, Print_line));
            textBox3.Clear();
        }

        private void On_exit(object sender, EventArgs e)
        {
            Save_threads_file();
            Print_line("Program exiting...");
        }

        private void Numeric_up_down_value_changed(object sender, EventArgs e)
        {
            tray_menu.MenuItems[0].Text = "Monitor - " + ud_Interval.Value + " sec";
        }
    }
}
