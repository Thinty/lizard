using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using LizardAPI;

namespace WinForms_GUI
{
    /// <summary>
    /// Maintains a list of threads (stored as MonitoredThread instances), which can be monitored for updates.
    /// </summary>
    public class ThreadMonitor
    {
        public BindingList<MonitoredThread> Threads;

        public int Threads_with_unread_replies { get { return Threads.Count(t => t.New_reply_count > 0); } }

        /// <summary>
        /// Returns the a list of references to threads which have unread replies.
        /// </summary>
        private List<MonitoredThread> Unread_threads
        {
            get { return Threads.Where(t => t.New_reply_count > 0).ToList(); }
        }

        public List<string> Unread_thread_urls
        {
            get
            {
                var urls = Unread_threads.Select(t => t.Oldest_new_reply_url).Distinct().ToList();
                return urls;
            }
        }

        public List<string> Unread_thread_names
        {
            get
            {
                var names = Unread_threads.Select(t => t.Name).ToList();
                return names;
            }
        }

        public List<MonitoredThread> Dead_threads { get { return Threads.Where(t => !t.Thread_up).ToList(); } }

        /// <summary>
        /// Initializes the object with an empty list of monitored threads.
        /// </summary>
        public ThreadMonitor()
        {
            Threads = new BindingList<MonitoredThread>();
        }

        /// <summary>
        /// Adds a thread to the list of monitored threads.
        /// </summary>
        public void Add_thread(MonitoredThread thread)
        {
            Threads.Add(thread);
        }

        /// <summary>
        /// Calls the Refresh action of each thread that is being monitored.
        /// </summary>
        public void Refresh_all_threads()
        {
            // Do the refreshes in parallel, so that they don't block each other
            Parallel.ForEach(Threads, t => t.Refresh());
        }

        /// <summary>
        /// Updates the "last seen by user" property of each monitored thread to the given value.
        /// </summary>
        public void Update_last_seen_by_user(long timestamp)
        {
            foreach (var t in Threads) t.Last_seen_by_user = timestamp;
        }
    }
}