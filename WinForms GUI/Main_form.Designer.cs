﻿namespace WinForms_GUI
{
    sealed partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tc_Help = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dg_Threads = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Add_thread = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tb_Log = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tb_Help = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flp_Monitor_controls = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.ud_Interval = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Force_check = new System.Windows.Forms.Button();
            this.btn_Start_monitoring = new System.Windows.Forms.Button();
            this.btn_Stop_monitoring = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tc_Help.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_Threads)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.flp_Monitor_controls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ud_Interval)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tc_Help, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flp_Monitor_controls, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(942, 606);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tc_Help
            // 
            this.tc_Help.Controls.Add(this.tabPage1);
            this.tc_Help.Controls.Add(this.tabPage2);
            this.tc_Help.Controls.Add(this.tabPage3);
            this.tc_Help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_Help.Location = new System.Drawing.Point(0, 0);
            this.tc_Help.Margin = new System.Windows.Forms.Padding(0);
            this.tc_Help.Name = "tc_Help";
            this.tc_Help.SelectedIndex = 0;
            this.tc_Help.Size = new System.Drawing.Size(942, 577);
            this.tc_Help.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(934, 551);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Monitored threads";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.dg_Threads, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(928, 545);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // dg_Threads
            // 
            this.dg_Threads.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_Threads.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_Threads.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_Threads.Location = new System.Drawing.Point(3, 3);
            this.dg_Threads.Name = "dg_Threads";
            this.dg_Threads.Size = new System.Drawing.Size(922, 510);
            this.dg_Threads.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.btn_Add_thread, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBox3, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 516);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(928, 29);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btn_Add_thread
            // 
            this.btn_Add_thread.AutoSize = true;
            this.btn_Add_thread.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Add_thread.Location = new System.Drawing.Point(833, 3);
            this.btn_Add_thread.Name = "btn_Add_thread";
            this.btn_Add_thread.Size = new System.Drawing.Size(92, 23);
            this.btn_Add_thread.TabIndex = 0;
            this.btn_Add_thread.Text = "Add new thread";
            this.btn_Add_thread.UseVisualStyleBackColor = true;
            this.btn_Add_thread.Click += new System.EventHandler(this.On_add_new_thread);
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(3, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(824, 20);
            this.textBox3.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.tb_Log);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(934, 551);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Log";
            // 
            // tb_Log
            // 
            this.tb_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_Log.Location = new System.Drawing.Point(3, 3);
            this.tb_Log.Multiline = true;
            this.tb_Log.Name = "tb_Log";
            this.tb_Log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_Log.Size = new System.Drawing.Size(928, 545);
            this.tb_Log.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.tableLayoutPanel4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(934, 551);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Help";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tb_Help, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.pictureBox1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(928, 545);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tb_Help
            // 
            this.tb_Help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_Help.Location = new System.Drawing.Point(0, 0);
            this.tb_Help.Margin = new System.Windows.Forms.Padding(0);
            this.tb_Help.Multiline = true;
            this.tb_Help.Name = "tb_Help";
            this.tb_Help.ReadOnly = true;
            this.tb_Help.Size = new System.Drawing.Size(928, 212);
            this.tb_Help.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::WinForms_GUI.Properties.Resources.lizard_laugh;
            this.pictureBox1.Location = new System.Drawing.Point(214, 212);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 333);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // flp_Monitor_controls
            // 
            this.flp_Monitor_controls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_Monitor_controls.AutoSize = true;
            this.flp_Monitor_controls.Controls.Add(this.label1);
            this.flp_Monitor_controls.Controls.Add(this.ud_Interval);
            this.flp_Monitor_controls.Controls.Add(this.label2);
            this.flp_Monitor_controls.Controls.Add(this.btn_Force_check);
            this.flp_Monitor_controls.Controls.Add(this.btn_Start_monitoring);
            this.flp_Monitor_controls.Controls.Add(this.btn_Stop_monitoring);
            this.flp_Monitor_controls.Location = new System.Drawing.Point(482, 577);
            this.flp_Monitor_controls.Margin = new System.Windows.Forms.Padding(0);
            this.flp_Monitor_controls.Name = "flp_Monitor_controls";
            this.flp_Monitor_controls.Size = new System.Drawing.Size(460, 29);
            this.flp_Monitor_controls.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Update interval:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ud_Interval
            // 
            this.ud_Interval.AutoSize = true;
            this.ud_Interval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ud_Interval.Location = new System.Drawing.Point(85, 3);
            this.ud_Interval.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.ud_Interval.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.ud_Interval.Name = "ud_Interval";
            this.ud_Interval.Size = new System.Drawing.Size(53, 20);
            this.ud_Interval.TabIndex = 0;
            this.ud_Interval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ud_Interval.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.ud_Interval.ValueChanged += new System.EventHandler(this.Numeric_up_down_value_changed);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(138, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "sec";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_Force_check
            // 
            this.btn_Force_check.Location = new System.Drawing.Point(168, 3);
            this.btn_Force_check.Name = "btn_Force_check";
            this.btn_Force_check.Size = new System.Drawing.Size(75, 23);
            this.btn_Force_check.TabIndex = 6;
            this.btn_Force_check.Text = "Check now";
            this.btn_Force_check.UseVisualStyleBackColor = true;
            this.btn_Force_check.Click += new System.EventHandler(this.Refresh_monitored_threads);
            // 
            // btn_Start_monitoring
            // 
            this.btn_Start_monitoring.AutoSize = true;
            this.btn_Start_monitoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Start_monitoring.Location = new System.Drawing.Point(249, 3);
            this.btn_Start_monitoring.Name = "btn_Start_monitoring";
            this.btn_Start_monitoring.Size = new System.Drawing.Size(112, 23);
            this.btn_Start_monitoring.TabIndex = 2;
            this.btn_Start_monitoring.Text = "Start monitoring";
            this.btn_Start_monitoring.UseVisualStyleBackColor = true;
            this.btn_Start_monitoring.Click += new System.EventHandler(this.Start_monitoring);
            // 
            // btn_Stop_monitoring
            // 
            this.btn_Stop_monitoring.AutoSize = true;
            this.btn_Stop_monitoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Stop_monitoring.Enabled = false;
            this.btn_Stop_monitoring.Location = new System.Drawing.Point(367, 3);
            this.btn_Stop_monitoring.Name = "btn_Stop_monitoring";
            this.btn_Stop_monitoring.Size = new System.Drawing.Size(90, 23);
            this.btn_Stop_monitoring.TabIndex = 3;
            this.btn_Stop_monitoring.Text = "Stop monitoring";
            this.btn_Stop_monitoring.UseVisualStyleBackColor = true;
            this.btn_Stop_monitoring.Click += new System.EventHandler(this.Stop_monitoring);
            // 
            // timer
            // 
            this.timer.Interval = 5000;
            this.timer.Tick += new System.EventHandler(this.Refresh_monitored_threads);
            // 
            // Main_Form
            // 
            this.AcceptButton = this.btn_Add_thread;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 606);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main_Form";
            this.Text = "Lizard ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.On_exit);
            this.Resize += new System.EventHandler(this.On_resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tc_Help.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_Threads)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.flp_Monitor_controls.ResumeLayout(false);
            this.flp_Monitor_controls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ud_Interval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tc_Help;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tb_Log;
        private System.Windows.Forms.FlowLayoutPanel flp_Monitor_controls;
        private System.Windows.Forms.Button btn_Start_monitoring;
        private System.Windows.Forms.Button btn_Stop_monitoring;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView dg_Threads;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btn_Add_thread;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ud_Interval;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox tb_Help;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_Force_check;

    }
}

