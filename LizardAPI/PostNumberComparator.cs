using System.Collections.Generic;

namespace LizardAPI
{
    public class PostNumberComparator : IEqualityComparer<Reply>
    {
        public bool Equals(Reply x, Reply y)
        {
            return x.Post_no == y.Post_no;
        }

        public int GetHashCode(Reply obj)
        {
            return obj.Post_no;
        }
    }
}