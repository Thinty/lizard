﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LizardAPI
{
    public class Version_Utilities
    {
        /// <summary>
        /// Inspects the given assembly and produces a string representation of the version. Assumes that the 3rd component of version is Delta Days since 2000, and fourth component is HHmm of build.
        /// </summary>
        /// <param name="assembly">Can be obtained with Assembly.GetExecutingAssembly()</param>
        /// <returns>Something like 1234-12-23 build 5601</returns>
        public static string Extract_version_string(Assembly assembly)
        {
            var version = assembly.GetName().Version;

            var delta_days = version.Build;
            var build = version.Revision;
            var build_date = new DateTime(2000, 1, 1).AddDays(delta_days - 1);
            
            var version_string = build_date.ToString("yyyy-MM-dd") + " build " + build;
            return version_string;
        }
    }
}
