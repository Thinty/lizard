﻿using System;
using Newtonsoft.Json;

namespace LizardAPI
{
    /// <summary>
    /// For automatically converting 0/1 to true/false. Taken from http://stackoverflow.com/a/14428145
    /// </summary>
    public class BoolConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((bool)value) ? 1 : 0);
        }

        public override object ReadJson(JsonReader reader, Type object_type, object existing_value, JsonSerializer serializer)
        {
            return reader.Value.ToString() == "1";
        }

        public override bool CanConvert(Type object_type)
        {
            return object_type == typeof(bool);
        }
    }
}
