using Newtonsoft.Json;

namespace LizardAPI
{
    /// <summary>
    /// JSON currently based on https://github.com/vichan-devel/vichan-API/
    /// </summary>
    public class Reply
    {
        [JsonProperty("no")]
        public int Post_no;

        [JsonProperty("sub")]
        public string Subject;
        public bool Has_subject { get { return !string.IsNullOrEmpty(Subject); } }

        [JsonProperty("com")]
        public string Text;
        public string Text_head { get { return Text.Length < 250 ? Text : Text.Substring(0, 250); } }

        [JsonProperty("time")]
        public long Timestamp;

        [JsonProperty("closed"), JsonConverter(typeof(BoolConverter))]
        public bool Closed;

        [JsonProperty("resto")]
        public int Replies_to;
    }
}