namespace LizardAPI
{
    public struct ReplyCount
    {
        public int New_replies;
        public int Deleted_replies;
    }
}