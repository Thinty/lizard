using System;

namespace LizardAPI
{
    public static class DateTimeHelpers
    {
        private static DateTime Epoch { get { return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); } }

        public static DateTime From_unix_time(long unix_time)
        {
            var human_time = Epoch.AddSeconds(unix_time);
            return human_time;
        }

        public static long To_unix_time(this DateTime dt)
        {
            return Convert.ToInt64((dt.ToUniversalTime() - Epoch).TotalSeconds);
        }
    }
}