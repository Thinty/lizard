using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LizardAPI
{
    /// <summary>
    /// This class keeps track of a single 4chan thread (and optionally a specific reply within that thread).
    /// </summary>
    public class MonitoredThread
    {
        public long Last_seen_by_user;
        public DateTime Last_refreshed;

        public string Json_url;
        public string Json;

        public List<Reply> Replies;
        private Action<string> print_line;

        /// <summary>
        /// A string that represents the name for this thread. If a thread has a title, title will be used. Otherwise first 250 characters of thread text, and board code/thread number if no connection has been established yet.
        /// 
        /// For cosmetic purposes only! Not stable, and not unique - do not use as identifier.
        /// </summary>
        [DisplayName("Thread")]
        public string Name
        {
            get
            {
                if (Replies.Count <= 0) return "PLACEHOLDER"; // TODO: Should return /abc/123#456

                var r = Replies.First();
                return r.Has_subject ? r.Subject : r.Text_head;
            }
        }

        /// <summary>
        /// Url of the thread's JSON. Does not include anchors pointing to any replies within the thread.
        /// </summary>
        [DisplayName("URL")]
        public string Url { get; set; }

        /// <summary>
        /// False if the thread is known to have 404'd.
        /// </summary>
        [DisplayName("Up?")]
        public bool Thread_up { get; set; }


        /// <summary>
        /// Optional reply number. If given, only replies to that post will be considered. If null, all replies in the thread are considered. NOTE: Currently ignored, since there is no straightforward way to determine which post replied to which.
        /// </summary>
        [DisplayName("Reply"), Browsable(false)]
        public uint? Reply_of_interest { get; set; }

        /// <summary>
        /// Full url pointing to the post being monitored. Reconstructed from parsed data.
        /// </summary>
        [Browsable(false)]
        public string Full_url { get { return Reply_of_interest == null ? Url : Url + "#" + Reply_of_interest; } }

        /// <summary>
        /// Link to the first new reply.
        /// </summary>
        [Browsable(false)]
        public string Oldest_new_reply_url { get { return Url + "#" + Replies.First(r => r.Timestamp > Last_seen_by_user).Post_no; } }

        /// <summary>
        /// Number of replies that have a timestamp later than when they were last seen by the user.
        /// </summary>
        [DisplayName("New replies")]
        public int New_reply_count { get { return Replies.Count(r => r.Timestamp > Last_seen_by_user); } }

        /// <summary>
        /// Initializes a new thread watcher.
        /// </summary>
        /// <param name="url">URL pointing to the thread.</param>
        /// <param name="log_function">A function pointer for printing a string to the log as a new line.</param>
        public MonitoredThread(string url, Action<string> log_function)
        {
            print_line = log_function;

            Replies = new List<Reply>();

            var parts = url.Split('#');
            Url = parts[0];

            if (parts.Length > 1) Reply_of_interest = uint.Parse(parts[1]);

            Json_url = Url.Replace(".html", ".json");
            Thread_up = true;
        }

        private static List<Reply> Parse_json(string json)
        {
            var replies = new List<Reply>();

            var raw_replies = JObject.Parse(json)["posts"];
            foreach (var raw_reply in raw_replies)
            {
                var reply = new Reply();
                JsonConvert.PopulateObject(raw_reply.ToString(), reply);
                replies.Add(reply);
            }

            return replies;
        }

        public void Refresh()
        {
            if (!Thread_up) return; // No point checking if thread has 404'd

            var last_refreshed = Last_refreshed;
            Last_refreshed = DateTime.Now;

            var c = new HttpClient();

            var version = Version_Utilities.Extract_version_string(Assembly.GetExecutingAssembly());
            c.DefaultRequestHeaders.Add("User-Agent", "LizardAPI " + version + " (https://bitbucket.org/Thinty/lizard)");

            c.DefaultRequestHeaders.IfModifiedSince = last_refreshed;

            try
            {
                var response = c.GetAsync(Json_url).Result;

                switch (response.StatusCode)
                {
                    case HttpStatusCode.NotFound:
                        Thread_up = false;
                        return;
                    case HttpStatusCode.NotModified:
                        return;
                    case HttpStatusCode.OK:
                        Replies = Parse_json(response.Content.ReadAsStringAsync().Result);
                        break;
                    default:
                        throw new WebException("Got status " + response.StatusCode);
                }
            }
            catch (WebException ex)
            {
                print_line("Thread " + Full_url + " encountered an exception: " + ex.Message);

                throw;
            }
        }
    }
}